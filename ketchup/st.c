#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

#define pi 3.1415926
//define rho's and w's dependence on a;
//give value to Beta, dim and Alpha

int func(double t, const double y[], double f[], void *params1)
{

    (void)(t);
    double dim = *(double *)params1;
    double Alpha = 1;
    double Beta = 0.1;
    
    double rho = pow(y[3], -dim) * pow((1 + Beta * Beta * pow(log(y[3]), 2)), -1 / pi / Beta) * pow(y[3], -2 / pi * atan(Beta * log(y[3])));
    double w = 2 / pi / dim * atan(Beta * log(y[3]));



    f[0] = 0.5 * (pow(y[0], 2) + (y[1] - y[0]) * (y[1] - y[0])) / (2 * dim);
    f[1] = y[0] * y[1];/*- 0.5 * Alpha * exp(y[2]) * rho+ 0.5 * dim * Alpha * exp(y[2]) * rho * w;*/
    f[2] = y[1];
    f[3] = y[3] * (y[1] - y[0]) / dim;

    return GSL_SUCCESS;
}

int jac(double t, const double y[], double *dfdy, double dfdt[], void *params1)
{
    (void)(t);
    double dim = *(double *)params1;
    double Alpha = 1;
    double Beta = 0.1;

    gsl_matrix_view dfdy_mat = gsl_matrix_view_array(dfdy, 4, 4);
    gsl_matrix *m = &dfdy_mat.matrix;

    gsl_matrix_set(m, 0, 0, 2 * y[0] - y[1]);
    gsl_matrix_set(m, 0, 1, -y[0] + y[1]);
    gsl_matrix_set(m, 0, 2, 0.0);
    gsl_matrix_set(m, 0, 3, 0.0);

    /*
    double rho = pow(y[3], -dim) * pow((1 + Beta * Beta * pow(log(y[3]), 2)), -1 / pi / Beta) * pow(y[3], -2 / pi * atan(Beta * log(y[3])));
    double w = 2 / pi / dim * atan(Beta * log(y[3]));*/

    /*
    double part1, part2, part3, part4;
    part1 = 2 * Beta * log(y[3]) * pow((1 + Beta * Beta * log(y[3]) * log(y[3])), -1 - 1 / pi / Beta) * pow(y[3], -1 - dim - 2 * atan(Beta * log(y[3])) / pi);
    part2 = pow((1 + Beta * Beta * log(y[3]) * log(y[3])), -1 / pi / Beta);
    part3 = (-dim - 2 / Beta * atan(Beta * log(y[3]))) / y[3] - 2 * Beta * log(y[3]) / (pi * (1 + Beta * Beta * log(y[3]) * log(y[3])) * y[3]);
    part4 = pow(y[3], -dim - 2 * atan(Beta * log(y[3]) / pi));
    double rho_deri = part1 + part2 * part3 * part4;
    double w_deri = 2 * Beta / (dim * pi * (1 + Beta * Beta * log(y[3]) * log(y[3]))) / y[3];
    */
    gsl_matrix_set(m, 1, 0, y[1]);
    gsl_matrix_set(m, 1, 1, y[0]);
    
    /*
    gsl_matrix_set(m, 1, 2, 0);
    gsl_matrix_set(m, 1, 3, 0);
    */

    /*
    gsl_matrix_set(m, 1, 2, 0.5 * Alpha * exp(y[2]) * rho * (1 - w * dim));
    gsl_matrix_set(m, 1, 3, 0.5 * Alpha * exp(y[2]) * (rho_deri * (1 - w * dim) - dim * rho * w_deri));*/


    
    gsl_matrix_set(m, 2, 0, 0.0);
    gsl_matrix_set(m, 2, 1, 1.0);
    gsl_matrix_set(m, 2, 2, 0.0);
    gsl_matrix_set(m, 2, 3, 0.0);

    gsl_matrix_set(m, 3, 0, -y[3] / dim);
    gsl_matrix_set(m, 3, 1, 1 / dim);
    gsl_matrix_set(m, 3, 2, 0.0);
    gsl_matrix_set(m, 3, 3, 0.0);

    dfdt[0] = 0.0;
    dfdt[1] = 0.0;
    dfdt[2] = 0.0;
    dfdt[3] = 0.0;

    return GSL_SUCCESS;
};

int main(void)
{
    double dim = 3;

    double u_in = 1, v_in = 0, a_in = 0, phi_in = 0;

    gsl_odeiv2_system sys = {func, jac, 4, &dim};

    gsl_odeiv2_driver *d =
        gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd,
                                      1e-6, 1e-6, 0);

    int i;
    double t = 0.0, t1 = 1;
    double y[4] = {u_in, v_in, a_in, phi_in};


    for (i = 1; i <= 100; i++)
    {
        double ti = i * t1 / 100.0;
        int status = gsl_odeiv2_driver_apply(d, &t, ti, y);

        if (status != GSL_SUCCESS)
        {
            printf("error, return value=%d\n", status);
            break;
        }

        printf("%.5e %.5e %.5e %.5e %.5e\n", t, y[0], y[1], y[2], y[3]);
    }

    gsl_odeiv2_driver_free(d);
    return 0;
}